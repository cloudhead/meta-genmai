FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PR := "${PR}.9"

COMPATIBLE_MACHINE_genmai = "genmai"

SRC_URI += "file://genmai-standard.scc \
            file://genmai-user-config.cfg \
            file://genmai-user-patches.scc \
            file://genmai-user-features.scc \
           "

# uncomment and replace these SRCREVs with the real commit ids once you've had
# the appropriate changes committed to the upstream linux-yocto repo
#SRCREV_machine_pn-linux-yocto_genmai ?= "19f7e43b54aef08d58135ed2a897d77b624b320a"
#SRCREV_meta_pn-linux-yocto_genmai ?= "459165c1dd61c4e843c36e6a1abeb30949a20ba7"

LINUX_VERSION = "3.8.13"
